import React from 'react';
import { View,Text,StyleSheet } from 'react-native';
import { red } from 'ansi-colors';

class DimensionesFlex extends React.Component{
    render(){
        return  (

            <View style={styles.container}>

                <Text style={styles.text1}>1er text</Text>
                <Text style={styles.text2}>2do text</Text>
                <Text style={styles.text3}>3er text</Text>

            </View>
            
        );
    }
}

export default DimensionesFlex;

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: "row",
        backgroundColor: '#cdcdcd',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text1: {
        width: 100,
        height: 100,
        backgroundColor: "yellow",
        color: "black",
    }, 
    text2: {
        width: 100,
        height: 100,
        backgroundColor: "blue",
        color: "black",
    }, 
    text3: {
        width: 100,
        height: 100,
        backgroundColor: "red",
        color: "black",
    }, 
});