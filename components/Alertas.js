import React from 'react';
import { View, Button, Alert, StyleSheet } from 'react-native';

class Alertas extends React.Component{

    handlePress = () => {
        Alert.alert("Soy el título", "Texto que se incluye en la alerta",
            [
             { text: "Botón 1", onPress: () => Alert.alert("presionaste uno")},   
             { text: "Botón 2", onPress: () => Alert.alert("presionaste dos")},   
            ]
        )
    }

    render(){
        return(

            <View style = { styles.container }>
                <Button title="Pínchame" onPress={ this.handlePress }/>
            </View>

        ); 
    }
}

export default Alertas; 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'gray',
        color: 'black'
    } 
});