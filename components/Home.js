import React from 'react';
import { StyleSheet, Text, View, } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Texto from './Texto';
import TextoState from './TextoState';

class Home extends React.Component {

    render(){
        return(      
            // Primera practica
            <View style = { styles.container } >
              <Text>Hola Mundo</Text> 

               {/* Segunda Práctica  --> props --> texto desde otro componente */}
              <Texto texto='OriVerhu' />

               {/* Tercera Práctica  --> State --> cambiar variables en eventos */}
               <TextoState  /> 

                 {/* Inventos */}
               <LinearGradient
                        colors={['#448AFF', '#9E9E9E', '#FFEB3B', '#FF5722']} >
                    <Text style = { styles.text }>We just need more colors</Text>
                </LinearGradient> 
            </View>

        );
    } 
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
       justifyContent: 'space-evenly', 
    },
    text: {
        color: "white",
        fontSize: 20,
    }, 
});

export default Home;