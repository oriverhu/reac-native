import React from 'react';
import { Text } from 'react-native';

class TextoState extends React.Component{
    // estado del componente   
    state = {
        texto: "Click Me"
    };

    handlePress = () => {
        
       // console.log(this.state.texto)
       var newtext='Click Me';
       if(this.state.texto=="Click Me"){newtext="Dbl Click Me";} 
        this.setState({texto: newtext})
    }

    render(){
        const { texto } = this.state;
        return  <Text onPress={this.handlePress} style={{ color: 'blue', textAlign: "center", fontSize: 30}} > { texto } </Text>;
    }
}

export default TextoState; 