import React from 'react';
import { Text, View, Button, Modal, StyleSheet, Image } from 'react-native';

class VentanaModal extends React.Component{

    state = { viewmodal: false }

    handlePress = () => {

        // if(this.state.viewmodal === false){newstate=true;}else{newstate=false;} 

        // this.setState( {viewmodal: newstate} )
        
        this.setState( {viewmodal: !this.state.viewmodal} ) // esto es lo mismo de arriba
    }

    render(){
        
        const { viewmodal } = this.state;

        return(

            <View style = { styles.container }>
                <Modal animationType="slide" visible={viewmodal} >
                    <View style= { { margin: 55 } } >
                        <Button title="Cerrar Modal" onPress={ this.handlePress } />
                    </View>
                    <Text style={{ textAlign: "center" }}>Esto es una modal con imagen</Text>
                    <Image  source={ require("./../assets/icon.png") } />
                </Modal>

                <Image style={ styles.picture } source={ { uri: 'https://static-cdn.jtvnw.net/jtv_user_pictures/xqcow-profile_image-9298dca608632101-300x300.jpeg' } } />
                <Button title="Abrir Modal" onPress={ this.handlePress }/>
            </View>

        ); 
    }
}

export default VentanaModal; 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'orange',
        color: 'white',
        flex: 1,        
        alignItems: 'center',
        justifyContent: 'center'
    }, 
    picture: {
        height:200, width:200,
    }
});