import React from 'react';
import { StyleSheet, View, Text, TextInput, Button, TouchableHighlight, TouchableOpacity } from 'react-native';

class Elements extends React.Component{
    
    state = {}

    handlePress = () => {
        if(this.state.text){
            alert(`Tu nombre es ${this.state.text} `);
        }else{
            alert(`Ingresa un Nombre`);
        }
    }

    handleChange = text => {
        this.setState({text});
    }

    render(){
        const { text } = this.state;
        return(
            <View style = { styles.container }>
                <TextInput placeholder="Ingresa Nombre" onChangeText={this.handleChange}></TextInput> 
                <Text>{ text && `Mi nombre es ${text}` }</Text>

                <Button title="Aceptar" onPress={this.handlePress}/>
                <TouchableHighlight onPress={this.handlePress}>
                    <Text style= {styles.button1}>Aceptar Highlight</Text>
                </TouchableHighlight>
                <TouchableOpacity onPress={this.handlePress}>
                    <Text style= {styles.button2}>Aceptar Opacity</Text>
                </TouchableOpacity>
            </View>
        ); 
    }
}

export default Elements; 

const styles = StyleSheet.create({
    button1:{
        backgroundColor: "green",
        padding: 15,
        color: "white"
    }, 
    button2:{
        backgroundColor: "black",
        padding: 15,
        color: "white"
    }, 
    container: {
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center'
    } 
});