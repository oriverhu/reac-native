import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { MapView } from 'expo'; 

class Mapas extends React.Component{

    render() {

        return (
            <View style={ styles.container } >
                <Text style={ { color: "white" } } > MAPA HOME</Text>
                <MapView
                    style={ { flex: 1 } }
                    initialRegion={{
                      latitude: -33.4534412,
                      longitude: -70.6993711,
                      latitudeDelta: 0.09,
                      longitudeDelta: 0.09
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'green',
        width: 600,
        height: 600
    }
}); 

export default Mapas;