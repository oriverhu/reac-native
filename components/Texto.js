import React from 'react';
import { Text } from 'react-native';

class Texto extends React.Component{
    render(){
        // propiedades de la clase del componente
        const { texto } = this.props

        return  <Text style={{ color: 'green', textAlign: "center"}}> { texto } </Text>;
    }
}

export default Texto;