import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { Location, Permissions } from 'expo'; 

class Geolocalization extends React.Component{

    state = {
        location: { coords: {} },
        errorMessage: null
    }

    getLocation =  async() => {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        if(status!=="granted"){
            return this.setState({ errorMessage: "Permiso no aceptado" });
        }

        const location = await Location.getCurrentPositionAsync();
       // console.log("location", location);
       this.setState( { location} ) 
    }

    render() {
        return (
            <View style={ styles.container } >
                <Text style= { { color: "white", fontSize: 30 } } >{ this.state.location.coords.latitude } { this.state.location.coords.longitude }</Text>
                 <Button title="Ubicación" onPress={ this.getLocation }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black'
    }
}); 

export default Geolocalization;