import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

class ListadoServer extends React.Component{
    
    constructor(props){
        super(props)
        this.fetchUsers()
    }

    state = {
        loading: true,
        users: []
    }

    fetchUsers = async () => {
      const response = await  fetch('https://jsonplaceholder.typicode.com/users');
      const pre = await response.json();
      const resp = pre.map( x => ({ ...x, key: String(x.id)})) // agregarle al json la propiedad key que será igual al id
      this.setState(
          {users:resp, loading: false} 
      );
    }

    render(){
        
        const { loading, users } = this.state;

        if(loading){
            return (
                <View>
                    <Text>
                        Cargando...
                    </Text>
                </View>
            );
        }

        return(

            <View style = { styles.container }>
                <FlatList
                    data={users}
                    renderItem={ ({item}) => (
                    <Text style={{fontSize: 20, textAlign: "center", padding: 2, color: "white" }}>{item.name}</Text>
                    ) }
                />
            </View>
        ); 
    }
}

export default ListadoServer; 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'purple',
        color: 'white'
    } 
});