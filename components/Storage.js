import React from 'react';
import { View, Button, StyleSheet, Alert, AsyncStorage } from 'react-native';
import Alertas from './Alertas';

class Storage extends React.Component{
    
    constructor(props){
        super(props)
       // this.traerDato();
    }

    traerDato = async() => {
        const dato = await AsyncStorage.getItem("dato");
        Alert.alert("Dato: ",dato);
    }

    handlePress = async() => { 
        await AsyncStorage.setItem("dato", "Este es mi dato en Storage");
    }

    render(){
        

        return(

            <View style = { styles.container }>
                <Button title="Asignar Valor" onPress={ this.handlePress }/>
                <Button title="Ver Valor" onPress={ this.traerDato }/>
            </View>

        ); 
    }
}


export default Storage; 

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        color: 'white'
    } 
});