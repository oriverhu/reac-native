import React from 'react';
import { View, ActivityIndicator } from 'react-native';

class Indicators extends React.Component{
    render(){
        return(

            <View>
                <ActivityIndicator size="large" color="#0000ff"/>
                <ActivityIndicator size="small" color="purple"/>
            </View>

        ); 
    }
}

export default Indicators; 