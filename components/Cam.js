import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { Permissions } from 'expo'; 
import { Camera } from 'expo-camera'

class Cam extends React.Component{

    state = {
        perm: null,
        type: Camera.Constants.Type.back
    }

    async componentDidMount(){ // como un on load
        const { status } = await Permissions.askAsync(Permissions.CAMERA)
        this.setState({ perm: status === 'granted' }) 
    }

    handleFlip = () => {
        const { Type } = Camera.Constants;
        const type = Type.back === this.state.type ? Type.front :  Type.back;
        this.setState( { type } );
    }

    // handlePicture = async() => {
    //     const x = await Camera.takePictureAsync( { base64: true } );
    //     console.log(x);
    // }
    handlePicture = async() => {
        try{
          const x = await Camera.takePictureAsync( { base64: true } );
          console.log(x);
        } catch (e) {
          console.log({ e })
        }
      }

    render() {

        const { perm } = this.state;

        if(perm === null){

            return <View />

        }else if( perm === false){

            return <View style={ styles.container }  > <Text>Dame Permisos de Cámara!!! </Text> </View>

        }else{

            return (
                <View style={ styles.container } >
                    <Camera style={ { flex: 1 } } type={ this.state.type } >
                        <View style={ { flex: 1, backgroundColor: 'transparent', flexDirection: "row", width:100, height: 300 } } >
                            <Button title="Flip" onPress={ this.handleFlip }></Button>
                            <Button title="Photo" onPress={ this.handlePicture }></Button>
                        </View>
                    </Camera>
                </View>
            );

        }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white'
    }
}); 

export default Cam;