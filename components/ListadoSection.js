import React from 'react';
import { View, Text, SectionList, StyleSheet } from 'react-native';

class ListadoSection extends React.Component{

    render(){

        return(
            <View style= { styles.container }>
                <SectionList 
                    sections={[
                        {  
                            title: "Section 1",
                            data: [
                                {name: "1", key:"1"},
                                {name: "1", key:"2"},
                                {name: "1", key:"3"},
                                {name: "1", key:"4"},
                                {name: "1", key:"5"},
                                {name: "1", key:"6"},
                                {name: "1", key:"7"},
                                {name: "1", key:"8"},
                            ]
                        },
                        {  
                            title: "Section 2",
                            data: [
                                {name: "2", key:"9"},
                                {name: "2", key:"10"},
                                {name: "2", key:"11"},
                                {name: "2", key:"12"},
                                {name: "2", key:"13"},
                                {name: "2", key:"14"},
                                {name: "2", key:"15"},
                                {name: "2", key:"16"},
                            ]
                        }
                    ]}
                    renderItem={ ({item}) => (
                        <Text style={{fontSize: 40, textAlign: "center", padding: 2 }}>{item.name}</Text>
                               )}
                    renderSectionHeader={ ({ section }) => (
                        <Text style={{fontSize: 40, textAlign: "center", padding: 2, backgroundColor: "red", textAlign: "left" }}>{section.title}</Text>
                    ) }                               
                /> 
            </View>
        ); 
    }
}

export default ListadoSection; 
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'gray',
    } 
});