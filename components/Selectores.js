import React from 'react';
import { View, Picker } from 'react-native';

class Selectores extends React.Component{
    state = {};
    render(){
        return(

            <View>
                <Picker                
                    selectedValue={this.state.value || 'id_2'} 
                    style={{ height: 70, width: 150 }}
                    onValueChange={
                        (value, idx) =>{
                            this.setState({value});
                        }
                    }
                >
                    <Picker.Item label="Ori" value="id_1" />
                    <Picker.Item label="Verhu" value="id_2" />
                </Picker>
            </View>

        ); 
    }
}

export default Selectores; 