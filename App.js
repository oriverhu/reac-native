import React from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { LinearGradient } from 'expo';
import Texto from './components/Texto';
import TextoState from './components/TextoState';
import Elements from './components/Elements';
import ListadoFlat from './components/ListadoFlat';
import ListadoSection from './components/ListadoSection';
import DimensionesFlex from './components/DimensionesFlex';
import ListadoServer from './components/ListadoServer';
import Selectores from './components/Selectores';
import Indicators from './components/Indicators';
import Alertas from './components/Alertas';
import VentanaModal from './components/VentanaModal';
import Storage from './components/Storage';
import Geolocalization from './components/Geolocalization';
import Cam from './components/Cam';
import Mapas from './components/Mapas';

export default class App extends React.Component {

    render(){
        return(      
            
            <ScrollView style = { styles.container } >


              {/* 11va Práctica  --> Alertas  */}
              <Alertas />

              {/* 12va Práctica  --> Modal  */}
              <VentanaModal />

              {/* 13 Práctica  --> Datos en Storage  */}
              <Storage />

              {/* 14 Práctica  --> acceso al gps  */}
              <Geolocalization />

              {/* 15 Práctica  --> acceso a la cámara  */}
              <Cam />

              {/* 15 Práctica  --> acceso a Mapas  */}
              <Mapas />

              {/* 1ra Práctica  --> simple texto */}
              <Text style={{ color: 'red', textAlign: "center"}}>Hola Mundo</Text> 

               {/* 2da Práctica  --> props --> texto desde otro componente */}
              <Texto texto='OriVerhu'  />

               {/* 3ra Práctica  --> State --> cambiar variables en eventos */}
               <TextoState /> 

                 {/* Inventos */}
               <LinearGradient
                        colors={['#448AFF', '#9E9E9E', '#FFEB3B', '#FF5722']} >
                    <Text style = { styles.text }>We just need more colors</Text>
                </LinearGradient>  

                 {/* 4ta Práctica  --> dimensionamiento con flex  */}
                <DimensionesFlex />

                 {/* 5ta Práctica  --> elementos  */}
                 <Elements />
                 
                 {/* 6ta Práctica  --> listado  */}
                 <ListadoFlat />

                {/* 7ma Práctica  --> listado extenso  */}
                <ListadoSection />

                {/* 8vo Práctica  --> listado desde server  */}
                <ListadoServer />

                {/* 9na Práctica  --> Selectores  */}
                <Selectores />

                {/* 10ma Práctica  --> Indicadores de espera   */}
                <Indicators />

            </ScrollView>

        );
    } 
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#dedede',
        paddingTop: 25,
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: 30,
    }, 
});